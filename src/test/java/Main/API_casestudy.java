package Main;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import Helper.helper_class;


public class API_casestudy {

	helper_class obj1 = new helper_class();
	
	@Test
    public void test() throws IOException, EncryptedDocumentException, NullPointerException, NoSuchAlgorithmException{
		
      ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ankit.xlsx").getFile());
		
		XSSFWorkbook wb = new XSSFWorkbook(file);
		XSSFSheet sheet = wb.getSheetAt(0);
		int rowstart = sheet.getFirstRowNum();
		int rowend = sheet.getLastRowNum();
		rowend++;
		//System.out.println(rowend);
		for(int i= rowstart+1;i<rowend;i++)
		{
			Row row = sheet.getRow(i);
	
			String url = row.getCell(0).getStringCellValue();
			String command = row.getCell(1).getStringCellValue();
			String key = row.getCell(2).getStringCellValue();
			String var1 = row.getCell(3).getStringCellValue();
			String var2 = row.getCell(4).getStringCellValue();
			String var3 = row.getCell(5).getStringCellValue();
			String var4 = row.getCell(6).getStringCellValue();
			String var5 = row.getCell(7).getStringCellValue();
			String var6 = (String) row.getCell(8).toString();
			int var7 = (int) row.getCell(9).getNumericCellValue();
			int var8 = (int) row.getCell(10).getNumericCellValue();
			int statusCode = (int) row.getCell(11).getNumericCellValue();

			
			String token = sheet.getRow(2).getCell(12).getStringCellValue();
			if(command.equals("save_user_card"))
			{
				helper_class.save_card(url,command,key,var1,var2,var3,var4,var5,var6,var7,var8);
			}  
			
		    if(command.equals("get_user_cards"))
			{
				helper_class.get(url, command, key, var1);
			} 
			if(command.equals("delete_user_card"))
			{
				helper_class.delete(url, command, key, var1,token);
			}    
			//System.out.println(token);
			
		}
	}
	
}
