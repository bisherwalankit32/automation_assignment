package Helper;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash_class {

	public static String encrypt(String str) throws NoSuchAlgorithmException
    { 
        try {  
            MessageDigest md = MessageDigest.getInstance("SHA-512"); 
  
            byte[] messageDigest = md.digest(str.getBytes()); 
  
            BigInteger no = new BigInteger(1, messageDigest); 
   
            String hashtext = no.toString(16); 
  
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
  
            return hashtext; 
        } 

        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
    }
	public static void main(String args[]) throws NoSuchAlgorithmException
	{
		String s= encrypt("smsplus|get_user_cards|smsplus:tpo|1b1b0");
		System.out.println(s);
		
	}
	
}
