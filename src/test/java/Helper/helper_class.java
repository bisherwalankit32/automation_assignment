package Helper;

import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class helper_class {

	public static Response save_card(String url, String command, String key,String var1, String var2, String var3,
			String var4, String var5, String var6, int var7, int var8) throws NoSuchAlgorithmException, IOException
	{
		 
		 String s = key+"|"+command+"|"+var1+"|1b1b0";
		 String hash = Hash_class.encrypt(s);
		
		 RestAssured.baseURI = url;
		 Response response =  RestAssured.given().urlEncodingEnabled(true)
				 .param("command", command)
				 .param("key", key)
				 .param("hash", hash)
				 .param("form", 2)
				 .param("var1", var1)
				 .param("var2", var2)
				 .param("var3", var3)
				 .param("var4", var4)
				 .param("var5", var5)
				 .param("var6", var6)
				 .param("var7", var7)
				 .param("var8", var8)
				 .post()
				 .then()
				 .statusCode(200)
				 .extract()
				 .response();
		 
		 JsonPath js = response.jsonPath();
		 
		 String token = js.get("cardToken");
		 
		 //System.out.println("Card token is "+token);           
		 String ans = response.getBody().asString();
		 System.out.println(ans);
		 
		
		 String path="/Users/ankit.bisherwal/Downloads/ankit.xlsx";
		  FileInputStream file= new FileInputStream(path);
		  XSSFWorkbook workbook= new XSSFWorkbook(file);
		  XSSFSheet sh =workbook.getSheet("sheet1");
		  
		sh.getRow(2).createCell(12).setCellValue(token);
		FileOutputStream fout=new FileOutputStream(path);
		workbook.write(fout);
		workbook.close();

		 return response;
		
	}
	
	public static Response get(String url, String command, String key, String var1) throws NoSuchAlgorithmException
	{
		 String s = key+"|"+command+"|"+var1+"|1b1b0";
		 String hash = Hash_class.encrypt(s);

		
		 RestAssured.baseURI = url;
		 Response response =  RestAssured.given().urlEncodingEnabled(true)
				 .param("command", command)
				 .param("key", key)
				 .param("hash", hash)
				 .param("form", 2)
				 .param("var1", var1)
				 .post()
				 .then()
				 .statusCode(200)
				 .extract()
				 .response();
				 System.out.println(response.asString());
				 
				 JsonPath js = response.jsonPath();
				 
				 String token = js.get("card_token");
				 return response;
	}
	
	public static void delete(String url, String command, String key, String var1, String token) throws NoSuchAlgorithmException
	{
		 String s = key+"|"+command+"|"+var1+"|1b1b0";
		 String hash = Hash_class.encrypt(s);
		 RestAssured.baseURI = url;
		 Response response =  RestAssured.given().urlEncodingEnabled(true)
				 .param("command", command)
				 .param("key", key)
				 .param("hash", hash)
				 .param("form", 2)
				 .param("var1", var1)
				 .param("var2", token)
				 .post()
				 .then()
				 .statusCode(200)
				 .extract()
				 .response();
				 System.out.println(response.getBody().asString());
				 
				 
	}
	
}
